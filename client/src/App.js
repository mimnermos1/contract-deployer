import React, { useState, useEffect } from "react";
import getWeb3 from "./getWeb3";
import TabsComponent from "./TabsComponent";
import "./App.css";
import logo from "./logo.png";
import { createTheme, ThemeProvider } from "@mui/material";
import {makeStyles} from '@mui/styles';
import Menlo from './Assets/Fonts/Menlo-Bold.ttf';
import { CButton } from "./components/CButton";

const theme = createTheme({
  typography: {
    color: 'black',
    fontFamily: [
      'Menlo',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        body: {
        }
      }
    }
  },
  components:{
    MuiScopedCssBaseline:{
      styleOverrides:`
        @font-face{
          font-family:'Menlo';
          font-style:normal;
          font-display: swap;
          font-weight: 400;
          src: local('Menlo'), local('Menlo-Bold'), url(${Menlo}) format('ttf');
          unicodeRange: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF;
        }
      `,
    }
  },
  palette:{
    primary:{
      main:'#FFFFFF',
    },
    secondary:{
      main:'#00000'
    },
    dark:{
      main:'#000'
    },
    error:{
      main:'#cc0000'
    },
    action:{
      disabledBackground:'red',
      disabled:'#8C8C8C',
    },
  },
  shape:{
    borderRadius:{
      round:50,
      small:10
    }
  },
});

const useStyles = makeStyles((theme) => ({
  root:{
    textAlign: 'left',
    backgroundColor: '#000000',
    justifyContent: 'center',
    width: '100vw',
    spacing: 0,
    justify: 'space-around',
    minHeight:'100vh'
  },
  header: {
    display: 'flex',
    flexDirection: 'column',
    fontSize: 'calc(10px + 2vmin)',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
  
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginRight:'5%',
      marginLeft:'5%',
    },
  },
  buttonRow: {
    margin: '2%',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
    gap: theme.spacing(2),
    marginBottom: theme.spacing(6),
    [theme.breakpoints.up('sm')]: {
      justifyContent: 'flex-end',
      marginBottom: 0,
    },
  },
  button: {
    height: '50%',
    margin: theme.spacing(2),
  },
  tabsContainer: {
    width: "80%",
    margin: "auto",
  },
  footer: {
    color: "white" ,
    paddingBottom:'2%',
    marginTop:'3.5%',
    fontSize: 'calc(16px - 0.1vmin)',
  },
  image: {
    width: '25rem',
    height: 'auto', 
  }
}))

const Content = (props) => {
    const classes = useStyles();
    const [state, setState] = useState({ web3: null, accounts: null, chainError: false});

    const subscribeProvider = async (provider) => {
        if (!provider.on) {
          return;
        }
        provider.on("close", () => setState({chainError:false, web3: null, acounts: null}));
        provider.on("accountsChanged", async (accounts: string[]) => {
           setState({chainError:false, web3: null, acounts: null})
        });
        provider.on("chainChanged", async (chainId: number) => {
           setState({chainError:false, web3: null, acounts: null})
        });

        provider.on("networkChanged", async (networkId: number) => {
           setState({chainError:false, web3: null, acounts: null})
        });
    }

    const fetchData = async () => {
        try{
            const web3 = await getWeb3();
            console.log(web3);
            const accounts = await web3.eth.getAccounts();
            //const provider = await web3Modal.connect();
            await subscribeProvider(web3.currentProvider);
            const chainId = await web3.eth.net.getId();
            // temporarily disable chainId 1 and chainId 137 whitelist
            setState({chainError: false, state, web3: web3, accounts: accounts}); 
            console.log("Content.fetchData: state.chainError = "+state.chainError);
            console.log("Content.fetchData: chainId = "+chainId);
            console.log(accounts);
        }catch (error){
            alert(error);
            console.error(error); 
        }
   }
   
    const disconnectWallet = async () => {
        if (web3 && web3.currentProvider && web3.currentProvider.close) {
            await web3.currentProvider.close();
        }
        //await web3Modal.clearCachedProvider();
        setState({chainError: false, web3: null, accounts:null}); 
    }

    const donate = async () => {
       await state.web3.eth.sendTransaction({from:state.accounts[0],to:process.env.REACT_APP_DONATE_ADDR, value:state.web3.utils.toWei("0.025", "ether")}); 
    }

    const isWalletConnected = state.accounts && state.accounts.length;

    return (
      <div className={classes.root}>
        <header className={classes.header}>
            <h1> FREEDEPLOY </h1>
            <div className={classes.buttonRow}>
              <CButton variant='normal' disabled={!isWalletConnected} onClick={donate} >
                Donate
              </CButton>
              <CButton
                variant='normal' 
                onClick={isWalletConnected ? disconnectWallet : fetchData}
              >
                {isWalletConnected ? 'DISCONNECT' : 'CONNECT WALLET' }
              </CButton>
            </div>
        </header>
        <div className={classes.tabsContainer}>
          <TabsComponent
            accounts={state.accounts}
            web3={state.web3} 
            chainError={state.chainError}
          />
        <footer className={classes.footer}>
            Experimental dapp, handle with care! Not financial advice. Any possible loss of money is your responsibility. View code <a style={{color: "white"}} href={process.env.REACT_APP_CODE_URL}>here</a>.
        </footer>
          
        </div>
      </div>
    );
}

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Content/>
    </ThemeProvider>
  )
}
export default App;
