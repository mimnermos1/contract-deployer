import React, { Component } from "react";
import { Box } from "@mui/system";
import { CircularProgress } from '@mui/material';
import { CInput } from "./components/CInput";

import "./App.css";
import { CButton } from "./components/CButton";
import { CModal } from "./components/CModal";
import { Message } from "./components/Message";

class MintFormComponent extends Component {
    state = { isMinting: false, metadataURL: '', metadataError: false, 
        modal: false, mintError: false, transactionHash: null,
        royaltiesError: false, royaltiesAmount: 10 

    };

    componentDidMount = async () => {
        this.handleURLChange = this.handleURLChange.bind(this);
        this.handleMint = this.handleMint.bind(this);
        this.handleError = this.handleError.bind(this);
        this.handleTxHash = this.handleTxHash.bind(this);
        this.handleConfrimation = this.handleConfirmation.bind(this);
        this.handleRoyaltiesChange = this.handleRoyaltiesChange.bind(this);
    }

    handleURLChange = (event) => {
        this.setState({ metadataURL: event.target.value, metadataError: !event.target.value }); 
    }

    countDecimals = (value) => {
        if (!!value && Math.floor(value) != value) {
            return value.toString().split(".")[1].length || 0;
        }
        return 0; 
    }

    handleRoyaltiesChange = (event) => {
        let regex = /^\s*-?\d+(\.\d{1})?\s*$/;
        this.setState({ royaltiesAmount: event.target.value, royaltiesError: 
            !event.target.value || 
            !regex.test(event.target.value) ||
            Number(event.target.value) > 100 || 
            Number(event.target.value) < 0 //|| this.countDecimals(Number(event.target.value)*10) > 1
        }); 
    }
    handleError = (error, receipt) => {
        console.log(error);
        this.setState({...this.state, mintError: true, isMinting: false});
    }

    handleTxHash = (hash) => {
        this.setState({transactionHash: hash});
    }

    handleConfirmation = (receipt) => {
        console.log(receipt);
        this.setState({...this.state,isMinting: false, modal: true, metadataURL: '', royaltiesAmount: 10});
    }

    handleMint = async (event) => {
        event.preventDefault();
        this.setState({...this.state, mintError: false});
        if(!this.state.metadataURL || (this.props.hasRoyalties && !this.state.royaltiesAmount)){
            this.setState({metadataError: !this.state.metadataURL, royaltiesError: !this.state.royaltiesAmount});
            return; 
        }
        this.setState({isMinting: true});
        if(this.props.hasRoyalties){
            await this.props.contract.methods.mint(this.props.accounts[0], 
                                                this.state.metadataURL, 
                                                this.props.accounts[0],
                                                this.state.royaltiesAmount*10)
                    .send({from: this.props.accounts[0]})
                .on('transactionHash', this.handleTxHash)
                .on('receipt', this.handleConfirmation)
                .on('error', this.handleError)

        }else{
            await this.props.contract.methods.mint(this.props.accounts[0], 
                                                this.state.metadataURL)
                    .send({from: this.props.accounts[0]})
                .on('transactionHash', this.handleTxHash)
                .on('receipt', this.handleConfirmation)
                .on('error', this.handleError)

 
        }
    }

    render() {
        const isFormComplete= this.state.metadataURL?.length>0 && (!this.props.hasRoyalties || !this.state.royaltiesError);

        const isWalletConnected = this.props.accounts && this.props.accounts.length

        return (
        <div>
            
            <form onSubmit={this.handleMint}>
                <CInput 
                    sx={{ display: 'block', mb: 3 }}
                    error={this.state.metadataError} 
                    helperText={this.state.metadataError? "Metadata URI is required to mint" : ""}
                    label="metadata URL" 
                    onChange={this.handleURLChange}
                    value={this.state.metadataURL}
                />
                { this.props.hasRoyalties &&
                    <CInput 
                        error={this.state.royaltiesError} 
                        helperText={this.state.royaltiesError? "Enter a valid royalties amount" : ""}
                        label="Royalties (%)" 
                        onChange={this.handleRoyaltiesChange}
                        value={this.state.royaltiesAmount}
                    />
                }
                <br></br>
                <br></br>
                <div >
                    {
                        this.state.isMinting &&
                        <CircularProgress sx={{ color:'black', marginBottom: '2rem', ml: 4, }} />
                    }
                    <br/>
                   {!this.state.isMinting && <CButton 
                        type='submit'
                        variant='alternative'
                        disabled={!isWalletConnected || !isFormComplete || this.props.chainError}
                    >
                        MINT!
                    </CButton>
                    }
                </div>
                {
                    this.state.isMinting &&
                    <Box sx={{ marginTop:3  }}>
                    <Message title="Minting..." />
                    </Box>
                }
            </form>
            { this.state.mintError && 
                (

                <Box sx={{ mt: 3 }}>
                    <Message variant='error' title='The transaction was rejected' onCancel={ () => this.setState({mintError: false})} /> 
                </Box>
                )
            }
            {
              !isWalletConnected && 
              (
                <Box sx={{ mt: 3 }}>
                  <Message title='Connect wallet to mint!!' variant='error' />
                </Box>
              )
            }
            {
              this.props.chainError && 
              (
                <Box sx={{ mt: 3 }}>
                  <Message title='Switch to Ethereum testnet to mint :)' variant='error' />
                </Box>
              )
            }
            <CModal title={'Minted!'+String.fromCodePoint("0x1F973")} open={this.state.modal} close ={()=> this.setState({ modal: false })}>
                <Box sx={{
                    fontSize: '1rem',
                    fontWeight: 500,
                    textAlign: 'center',
                    wordBreak: 'break-all',
                    mt: 6
                }}>
                    <p>
                    You have sucessfully minted your NFT.
            Please wait for it to appear on Opensea etc, it can sometimes take up to 2 hours. View it on <a href={"https://etherscan.io/tx/"+this.state.transactionHash} target="_blank">Etherscan</a>.</p>
                </Box>
            </CModal>
        </div>
        );
    }
}

export default MintFormComponent;
