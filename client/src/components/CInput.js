import React from "react";
import { TextField, styled } from '@mui/material';


const CustomInput = styled(TextField)({
    '& label.Mui-focused': {
      color: 'black',

    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'black',
        borderWidth:'4px',
        borderRadius: 10
      },
      '&.Mui-focused fieldset': {
        borderColor: 'black',
        borderWidth:'4px'

      },
    },
  });

const inputStyle={
  root:{
    "&.Mui-error":{
      color:"error.main"
    },
    "&.MuiFormHelperText-root":{
      color:"error.main"
    }
  }
}

export const CInput = (props) => {
    return(
        <CustomInput value={props.value} sx={{ margin: 0,inputStyle }} {...props} />
    )
}
