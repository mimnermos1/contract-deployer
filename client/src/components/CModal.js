import React, { Children } from "react";
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import { Box } from "@mui/system";


const modalStyle = {
    root:{
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        width: 'max(40%, 15rem)',
        minHeight:300,
        bgcolor: "primary.main",
        p: 4,
        boxShadow:'5px 5px  #fff'
    },
    title:{
        textAlign:'center',
        fontFamily:'Menlo',
        fontSize:20,
        fontWeight:'700'
    }
    
  };


export const CModal = (props) =>{
    const { children, title, open, close } = props;
    return (
        <Modal
            open={open}
            onClose={close}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            disableAutoFocus
        >
            <Fade in={open}>
                <Box sx={modalStyle.root} >
                    <Box sx={modalStyle.title}>{title}</Box>
                    <Box>
                        {children}
                    </Box>
                </Box>
            </Fade>
        </Modal>
    );
}