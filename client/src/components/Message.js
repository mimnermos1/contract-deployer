import React from "react";
import { Box } from "@mui/system";
import { makeStyles } from "@mui/styles";
import ClearIcon from '@mui/icons-material/Clear';
import AnnouncementIcon from '@mui/icons-material/Announcement';

export const Message = (props) =>{
    const { title, variant, onCancel,showCancel } = props;
    const classes = useStyle(props);

    const variantStyle = {
        "normal":classes.normal,
        "error":classes.error
    }
    const iconStyle = {
        "normal":classes.normalCross,
        "error":classes.errorCross
    }

    return(
        <div className={`${classes.root} ${variantStyle[variant||"normal"]}`}>
            <AnnouncementIcon sx={{ fontSize: '18px' }} className={iconStyle[variant || 'normal']}/>
            <div className={classes.text}>{title}</div>
            {
                showCancel && <ClearIcon onClick={onCancel} className={`${classes.crossIcon} ${iconStyle[variant || 'normal']}`} />
            }
        </div>
    )
}

const useStyle = makeStyles((theme) => ({
    root:{
        display:'flex',
        borderRadius:10,
        padding: `${theme.spacing(1)} ${theme.spacing(2)}`,
        justifyContent: 'space-between',
        alignItems:'center',
        fontFamily:'Menlo',
        position:'relative',
        fontWeight:'lighter',
        boxShadow:'3px 3px black',
    },
    text: {
        flex: 1,
        textAlign: 'center'
    },
    normal:{
        color:'black',
        background:'#FFFFFF'
    },
    error:{
        color:'white',
        background:'#cc0000'
    },
    crossIcon:{
        // position:'absolute',
        right:0,
        top:0,
    },
    errorCross:{
        color:"white"
    },
    normalCross:{
        color:"black"
    }
}))
