## freedeploy 

### Installation
Clone this project, then install and build in the project root directory
```
npm install
npm run build
```
Then move into the client directory and install
```
cd client/
npm install
```

### Running
to run locally, go into the client directory
```
cd client
```
and run local server
```
npm run start
```

To create a production build
```
cd client/
npm run build
```

You can now ship the build/ directory. This build is configured with IPFS upload in mind, which can be done by simply uploading it to IPFS like explained [here](https://docs.ipfs.io/how-to/websites-on-ipfs/multipage-website/#add-files-to-ipfs)

Read below for traditional web2 deployment.

### Extension
To run your own version on web2, you can use your own deploy method or use the provided aws infrastructure. For the latter, simply running `cdk deploy` under `infrastructure/` should create a cloudfront distribution for the website. What's left to do is setting up a domain for it.

The easiest way is forking the project under gitlab and using the existing CI configuration. You need to set a few environment variables outlined below.

For aws deployment:

- `AWS_ACCESS_KEY_ID`
- `AWS_DEFAULT_REGION`
- `AWS_SECRET_ACCESS_KEY`
- `DISTRIBUTION_ID`
- `S3_BUCKET`

The CI also supports (optional) an additional develop branch which deploys on another S3 bucket, which need the varibale:

- `S3_BUCKET_DEVELOP`

Same for another branch testnet:

- `S3_BUCKET_TESTNET`

For connecting with walletconnect, and infura id can be provided with:

- `INFURA_ID`

if not present, the website expects a provider to be present (for instance metamask which works out of the box).

website specific:
- `DONATE_ADDR` ethereum address to which funds are transferred when the "DONATE" button is pressed
- `CODE_URL` URL that points to your repository, it is linked in the footer


