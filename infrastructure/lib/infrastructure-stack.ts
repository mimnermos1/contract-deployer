import * as cdk from '@aws-cdk/core';
import * as s3 from '@aws-cdk/aws-s3';
import * as cloudfront from '@aws-cdk/aws-cloudfront';
import * as iam from '@aws-cdk/aws-iam';

export class InfrastructureStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const bucket = new s3.Bucket(this, 'ContractDeployerBucket', {
      bucketName: 'contract-deployer-cdk',
      websiteIndexDocument: 'index.html', // 1
    });

    const bucketDevelop = new s3.Bucket(this, 'ContractDeployerBucketDevelop', {
      bucketName: 'contract-deployer-cdk-develop',
      websiteIndexDocument: 'index.html', // 1
    });

    const bucketTestnet = new s3.Bucket(this, 'ContractDeployerBucketTestnet', {
      bucketName: 'contract-deployer-cdk-testnet',
      websiteIndexDocument: 'index.html', // 1
    });

    const bucketPolicy = new iam.PolicyStatement({
      actions: ['s3:GetObject'],
      resources: [
        `${bucketDevelop.bucketArn}/*`
      ],
      principals: [new iam.Anyone()],
    })
    bucketDevelop.addToResourcePolicy(bucketPolicy); //

    // CDN mainnet
    const cloudFrontOAI = new cloudfront.OriginAccessIdentity(this, 'OAI');

    const distribution = new cloudfront.CloudFrontWebDistribution(this, 'WemintDistribution', {
        originConfigs: [ 
            {
                s3OriginSource: {
                    s3BucketSource: bucket,
                    originAccessIdentity: cloudFrontOAI
                },
                behaviors: [{ isDefaultBehavior: true }]
            }
        ] 
    });

    bucket.grantRead(cloudFrontOAI.grantPrincipal);

    // CDN testnet
    const cloudFrontOAITestnet = new cloudfront.OriginAccessIdentity(this, 'OAI-testnet');

    const distributionTestnet = new cloudfront.CloudFrontWebDistribution(this, 'WemintDistributionTestnet', {
        originConfigs: [ 
            {
                s3OriginSource: {
                    s3BucketSource: bucketTestnet,
                    originAccessIdentity: cloudFrontOAITestnet
                },
                behaviors: [{ isDefaultBehavior: true }]
            }
        ] 
    });

    bucketTestnet.grantRead(cloudFrontOAITestnet.grantPrincipal);

    }
}

